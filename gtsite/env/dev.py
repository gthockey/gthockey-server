from .base import *

DEBUG = True
SECRET_KEY = 'WE_DONT_CARE'
ALLOWED_HOSTS = ["localhost", "127.0.0.1"]
FRONT_END_URL = "http://localhost:4200"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }
}
