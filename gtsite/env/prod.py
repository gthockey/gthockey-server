from .base import *

DEBUG = False
ALLOWED_HOSTS = [".gthockey.com"]

STATIC_ROOT = "/var/www/static"

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'gthockeysite'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

FRONT_END_URL = "https://gthockey.com"

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': '/var/www/django.log',
        }
    },
    'loggers': {
        'django': {
            'level': 'INFO',
            'handlers': ['file'],
            'propagate': True,
        }
    }
}

SPREADSHEET_ID = "1tPudc_iY44H6x9aety0TBvffurLXlBA_GPm0Z1Xoqh8"
PROSPECT_SEND_TO_SHEET = True
