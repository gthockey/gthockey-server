# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'gthockey',
    'corsheaders',
    'djoser',
)

MIDDLEWARE = (
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ROOT_URLCONF = 'gtsite.urls'
WSGI_APPLICATION = 'gtsite.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'America/New_York'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'

CORS_ORIGIN_ALLOW_ALL = True
CORS_URLS_REGEX = r'^/api/.*$'
CORS_ALLOW_HEADERS = '*'

# Specify SMTP server in local settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

APPEND_SLASH = True

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
    ],
}

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DATABASE_ENGINE','django.db.backends.postgresql'),
        'NAME': os.environ.get('DATABASE_NAME', ''),
        'USER': os.environ.get('DATABASE_USER', ''),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
        'HOST': os.environ.get('DATABASE_HOST', ''),
        'PORT': os.environ.get('DATABASE_PORT', ''),
    }
}

RECAPTCHA_PRIVATE_KEY = os.environ.get('RECAPTCHA_KEY', '')
SECRET_KEY = os.environ.get('SECRET_KEY', '')

GOOGLE_SHEETS_SERVICE_ACCOUNT = "./sheets_sa.json"
PROSPECT_SEND_TO_SHEET = False
SPREADSHEET_ID = "1KpahbuID1CKUtONrLJGbidsPimB5t48msojUylX_GS8"

MEDIA_URL = "http://localhost:8000/media/"
MEDIA_REL_URL = "/media/"
REPOSITORY_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
MEDIA_ROOT = os.path.join(REPOSITORY_ROOT, "media/")

STRIPE_SECRET_KEY = "sk_test_51JS6TvCN8seTnsw1hm7EUMzZConP4gVhbTWuySeGt9O0s6uzOt4ICRSi4QaZQ37gj6ZLTGWvYKIjNWlj2n9xgTc00003hYB7wA"

try:
    from .local import *
except ImportError:
    pass
