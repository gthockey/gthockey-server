# Generated by Django 2.0.2 on 2019-10-11 02:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gthockey', '0018_auto_20180606_2238'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='image',
            field=models.ImageField(null=True, upload_to='players'),
        ),
    ]
