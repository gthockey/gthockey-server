from django.conf import settings
from google.oauth2 import service_account
from googleapiclient.discovery import build

SCOPES = ["https://www.googleapis.com/auth/spreadsheets", ]

service = None


# Lazy load the service -- otherwise the service account file might not exist and this will fail.
def get_service():
    global service
    if not service:
        credentials = service_account.Credentials.from_service_account_file(
            settings.GOOGLE_SHEETS_SERVICE_ACCOUNT, scopes=SCOPES
        )
        service = build('sheets', 'v4', credentials=credentials)
    return service


def append_row(data_list):
    value_input_option = "RAW"

    insert_data_option = "INSERT_ROWS"

    sheet_range = "Not Sorted Yet!A:J"
    value_range_body = {
        "values": [data_list],
        "majorDimension": "ROWS"
    }

    request = get_service().spreadsheets().values().append(spreadsheetId=settings.SPREADSHEET_ID,
                                                           range=sheet_range,
                                                           valueInputOption=value_input_option,
                                                           insertDataOption=insert_data_option,
                                                           body=value_range_body)
    request.execute()
