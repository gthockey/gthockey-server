from django.http import JsonResponse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
import stripe

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from .forms import ContactForm, ProspectForm, MobileProspectForm, EmailListForm, OrderForm
from .models import Game, Season, Player, NewsStory, Board, Coach, Email, ShopItem, \
    ShopItemOptionList, ShopItemCustomOption
from .serializers import PlayerSerializer, GameSerializer, GameMinSerializer, ArticleSerializer, \
    BoardSerializer, CoachSerializer, ShopItemListSerializer, ShopItemSerializer, SeasonSerializer
from . import sheets


class PlayerList(APIView):
    def get(self, request):
        players = Player.objects.order_by('number')
        serializer = PlayerSerializer(players, many=True)
        return JsonResponse(serializer.data, safe=False)


class GameList(APIView):
    def get(self, request):
        params = request.query_params

        season_id = int(params.get('season', Season.get_current().id))
        season = Season.objects.get(pk=season_id)

        min_date_str = params.get('date_from', '2000-01-01')
        max_date_str = params.get('date_to', '2099-01-01')

        limit = int(params.get('limit', 1000))

        descending = 'desc' in params
        order = '-date' if descending else 'date'

        games = Game.objects.order_by(order) \
                    .filter(season=season) \
                    .filter(date__gte=min_date_str) \
                    .filter(date__lte=max_date_str) \
                    .prefetch_related('location') \
                    .prefetch_related('opponent')[:limit]

        gameserializer = GameMinSerializer
        if 'fulldata' in params:
            gameserializer = GameSerializer

        serializer = gameserializer(games, many=True)
        return JsonResponse(serializer.data, safe=False)


class GameDetail(APIView):
    def get(self, request, id):
        game = Game.objects.get(pk=id)
        serializer = GameSerializer(game)
        return JsonResponse(serializer.data, safe=False)


class SeasonList(APIView):
    def get(self, request):
        seasons = Season.objects.all().order_by('year')
        serializer = SeasonSerializer(seasons, many=True)
        return JsonResponse(serializer.data, safe=False)


class CurrentSeason(APIView):
    def get(self, request):
        season = Season.get_current()
        serializer = SeasonSerializer(season)
        return JsonResponse(serializer.data, safe=False)


class ArticleList(APIView):
    def get(self, request):
        articles = NewsStory.objects.order_by('-date')
        serializer = ArticleSerializer(articles, many=True)
        return JsonResponse(serializer.data, safe=False)


class ArticleDetail(APIView):
    def get(self, request, id):
        article = NewsStory.objects.get(pk=id)
        serializer = ArticleSerializer(article)
        return JsonResponse(serializer.data, safe=False)


class BoardList(APIView):
    def get(self, request):
        board = Board.objects.all().order_by('priority')
        serializer = BoardSerializer(board, many=True)
        return JsonResponse(serializer.data, safe=False)


class CoachList(APIView):
    def get(self, request):
        coaches = Coach.objects.all().order_by('priority')
        serializer = CoachSerializer(coaches, many=True)
        return JsonResponse(serializer.data, safe=False)


class ShopList(APIView):
    def get(self, request):
        params = request.query_params

        if 'brick_and_mortar' in params:
            items = ShopItem.objects.all().filter(visible=True, brick_and_mortar=True, ticket=True)
        else:
            items = ShopItem.objects.all().filter(visible=True, brick_and_mortar=False)

        serializer = ShopItemListSerializer(items, many=True)
        return JsonResponse(serializer.data, safe=False)


class ShopDetail(APIView):
    def get(self, request, id):
        try:
            item = ShopItem.objects.get(pk=id, visible=True)
            serializer = ShopItemSerializer(item)
            return JsonResponse(serializer.data, safe=False)
        except ObjectDoesNotExist:
            return JsonResponse({"errors": "Object Does Not Exist"}, status=404)


class ContactFormView(APIView):
    @csrf_exempt
    def post(self, request):
        form = ContactForm(request.data)
        if form.is_valid():
            subject = form.get_subject()
            sender = "GT Hockey"
            message = form.get_message()
            recipients = [e.email for e in Email.objects.all() if e.active]
            send_mail(subject, message, sender, recipients)
            return JsonResponse({}, status=200)
        return JsonResponse({"errors": form.errors}, status=400)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ContactFormView, self).dispatch(*args, **kwargs)


class ProspectFormView(APIView):
    @csrf_exempt
    def post(self, request):
        form = ProspectForm(request.data)
        if form.is_valid():
            subject = form.get_subject()
            sender = "gthockeysite@gmail.com"
            message = form.get_message()
            recipients = [e.email for e in Email.objects.all() if e.active]
            send_mail(subject, message, sender, recipients, fail_silently=False)

            if settings.PROSPECT_SEND_TO_SHEET:
                sheets.append_row(form.get_data_arr())
            return JsonResponse({}, status=200)
        return JsonResponse({"errors": form.errors}, status=400)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ProspectFormView, self).dispatch(*args, **kwargs)


class MobileProspectFormView(APIView):
    @csrf_exempt
    def post(self, request):
        form = MobileProspectForm(request.data)
        if form.is_valid():
            subject = form.get_subject()
            sender = "GT Hockey"
            message = form.get_message()
            recipients = [e.email for e in Email.objects.all() if e.active]
            send_mail(subject, message, sender, recipients)

            if settings.PROSPECT_SEND_TO_SHEET:
                sheets.append_row(form.get_data_arr())
            return JsonResponse({}, status=200)
        return JsonResponse({"errors": form.errors}, status=400)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(MobileProspectFormView, self).dispatch(*args, **kwargs)


class InvolvementFormView(APIView):
    @csrf_exempt
    def post(self, request):
        form = EmailListForm(request.data)
        if form.is_valid():
            subject = form.get_subject()
            sender = "GT Hockey"
            message = form.get_message()
            recipients = [e.email for e in Email.objects.all()]
            send_mail(subject, message, sender, recipients)
            return JsonResponse({}, status=200)
        return JsonResponse({"errors": form.errors}, status=400)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(InvolvementFormView, self).dispatch(*args, **kwargs)


class OrderFormView(APIView):
    @csrf_exempt
    def post(self, request):
        form = OrderForm(request.data)
        if form.is_valid():
            subject = form.get_subject()
            sender = "GT Hockey"
            message = form.get_message()
            recipients = [e.email for e in Email.objects.all()]
            send_mail(subject, message, sender, recipients)

            # Customer Email
            subject = form.get_customer_subject()
            sender = "GT Hockey"
            message = form.get_customer_message()
            recipients = [form.cleaned_data['email']]
            send_mail(subject, message, sender, recipients)
            return JsonResponse({}, status=200)
        return JsonResponse({"errors": form.errors}, status=400)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(OrderFormView, self).dispatch(*args, **kwargs)


class CreateCheckoutSession(APIView):
    @csrf_exempt
    def post(self, request):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        # TODO verify request format
        lineitems = [convert(item, False) for item in request.data]
        print(lineitems)
        session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=lineitems,
            mode='payment',
            shipping_address_collection={
                "allowed_countries": ["US"],
            },
            success_url=settings.FRONT_END_URL + '/cart/success',
            cancel_url=settings.FRONT_END_URL + '/cart/cancel',
        )
        return JsonResponse({"id": session.id}, status=200)


class CreateDonation(APIView):
    @csrf_exempt
    def post(selfself, request):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        amount = request.data["amount"]
        if not isinstance(amount, float) and not isinstance(amount, int):
            return JsonResponse({"errors": ["Non float input"]}, status=400)
        if amount <= 0:
            return JsonResponse({"errors": ["Negative amount"]}, status=400)
        donation_item = {
                'price_data': {
                    'currency': 'usd',
                    'unit_amount': amount * 100,
                    'product_data': {
                        'name': 'GT Hockey Donation',
                        'description': 'Thank you so much for your support!',
                        'images': [
                            # item.image.url,
                            #  When testing locally you can use the line below to render the image
                            #  properly on Stripe's checkout. (Otherwise the link refers to
                            #  localhost:8000 which stripe can't reach).
                            #
                            # "https://test.gthockey.com/media/" + item.image.name,
                        ],
                    },
                },
                'quantity': 1,
            }
        print(donation_item)
        session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=[donation_item],
            mode='payment',
            success_url=settings.FRONT_END_URL + '/cart/success',
            cancel_url=settings.FRONT_END_URL + '/cart/cancel',
        )
        return JsonResponse({"id": session.id}, status=200)


class CreatePaymentIntent(APIView):
    @csrf_exempt
    def post(self, request):
        stripe.api_key = settings.STRIPE_SECRET_KEY

        # Use this value to make the distinction between brick and mortar and mobile / web payments
        is_in_person_payment = request.data.get('in_person_payment', False)

        # Creating a customer for in person payment just requires name and email
        # Creating a customer for mobile or web payments requires name, email, and address
        if is_in_person_payment:
            customer = stripe.Customer.create(
                email=request.data['email'],
                name=request.data['name']
            )
        else:
            customer_address = {}
            customer_address['line1'] = request.data['address']['line1']
            customer_address['line2'] = request.data['address']['line2']
            customer_address['city'] = request.data['address']['city']
            customer_address['state'] = request.data['address']['state']
            customer_address['postal_code'] = request.data['address']['zip']
            customer_address['country'] = 'US'

            customer = stripe.Customer.create(
                email=request.data['email'],
                address=customer_address,
                name=request.data['name']
            )

        # Getting line items
        lineitems = [convert(item, True) for item in request.data['items']]

        # Tally up the total price of the cart and update the metadata for the payment intent
        totalPrice = 0
        paymentMetadata = {}
        line = 1
        for item in lineitems:
            totalPrice += item['price_data']['unit_amount']

            itemName = item['price_data']['product_data']['name']
            itemDescription = item['price_data']['product_data']['description']
            itemQuantity = str(item['quantity'])
            itemPrice = "${:,.2f}".format(float(item['price_data']['unit_amount'] / 100))
            metadataValue = "Quantity: {quantity}\n{price}\n{description}".format(
                quantity=itemQuantity, price=itemPrice, description=itemDescription)
            # Truncate the value at 500 characters as that is the maximum length stripe accepts
            paymentMetadata[str(line) + ". " + itemName] = metadataValue[:500]
            line += 1

        # Payment intents are creates slightly differently for payments in person vs payments online
        if is_in_person_payment:
            paymentIntent = stripe.PaymentIntent.create(
                amount=totalPrice,
                currency='usd',
                customer=customer.id,
                receipt_email=customer.email,
                metadata=paymentMetadata,
                payment_method_types=['card_present'],
                capture_method='manual'
            )
        else:
            paymentIntent = stripe.PaymentIntent.create(
                amount=totalPrice,
                currency='usd',
                customer=customer.id,
                receipt_email=customer.email,
                metadata=paymentMetadata,
                shipping={'address': customer['address'], 'name': customer['name']}
            )

        print(paymentIntent)

        return JsonResponse({"paymentIntent": paymentIntent.client_secret})


class CapturePaymentIntent(APIView):
    @csrf_exempt
    def post(self, request):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        payment_indent_id = request.data['intent_id']
        stripe.PaymentIntent.capture(payment_indent_id)
        return JsonResponse({"paymentIntent": payment_indent_id})


class ConnectionToken(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        connection_token = stripe.terminal.ConnectionToken.create()
        return JsonResponse({"secret": connection_token.secret}, status=200)


def convert(orderitem, fromMobile):
    # TODO what if not found
    print(orderitem)
    item = ShopItem.objects.get(pk=orderitem['item_id'])
    price = item.price
    quantity = orderitem.get('quantity', 1)
    metadata = {}
    for optid in orderitem['options']:
        if fromMobile:
            opt = ShopItemOptionList.objects.get(pk=optid['id'])
            metadata[opt.display_name] = optid['value']
        else:
            opt = ShopItemOptionList.objects.get(pk=optid)
            metadata[opt.display_name] = orderitem["options"][optid]
    for optid in orderitem['custom_options']:
        if fromMobile:
            opt = ShopItemCustomOption.objects.get(pk=optid['id'])
            price += opt.extra_cost
            metadata[opt.display_name] = optid['value']
        else:
            opt = ShopItemCustomOption.objects.get(pk=optid)
            price += opt.extra_cost
            metadata[opt.display_name] = orderitem["custom_options"][optid]
    if len(metadata) > 0:
        description = ", ".join(["%s: %s" % option for option in metadata.items()])
    else:
        description = item.description
    return {
        'price_data': {
            'currency': 'usd',
            'unit_amount': int(price * 100 * quantity),
            'product_data': {
                'name': item.name,
                'description': description,
                'images': [
                    item.image.url,
                    #  When testing locally you can use the line below to render the image
                    #  properly on Stripe's checkout. (Otherwise the link refers to
                    #  localhost:8000 which stripe can't reach).
                    #
                    # "https://test.gthockey.com/media/" + item.image.name,
                ],
                'metadata': metadata,
            },
        },
        'quantity': quantity,
    }


def handler404(request, exception):
    return JsonResponse({'message': 'API method does not exist'}, status=404)
