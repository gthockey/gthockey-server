import json
import os
import requests
import shutil
import urllib.request

from django.core.files import File
from django.core.management.base import BaseCommand

from gthockey.models import Board, Coach, Game, NewsStory, Player, Rink, Season, ShopItem, \
    ShopItemCustomOption, ShopItemImage, ShopItemOptionList, Team


class Command(BaseCommand):
    help = "Populates database using remote api"

    tmp_img_dir = "/tmp/django_images/"

    seen_teams = set()
    seen_rinks = set()

    @staticmethod
    def _have_image(image_url, image_field):
        return (image_url is not None) and (
                image_url.split("/")[-1] == str(image_field).split("/")[-1])

    def _get_image(self, image_url, image_field):
        if not image_url:
            return
        if self._have_image(image_url, image_field):
            print("Already have: %s" % image_url)
            return
        tmp_file = self.tmp_img_dir + "_".join(image_url.split("/")[-2:])
        file = image_url.split("/")[-1]
        print("Getting: %s" % image_url)
        urllib.request.urlretrieve(image_url, tmp_file)
        image_field.save(file, File(open(tmp_file, "rb")))

    def _update_opponent(self, opponent):
        team_id = opponent["id"]
        if team_id in self.seen_teams:
            return Team.objects.get(pk=team_id)
        print("Saving Team: %s" % opponent["school_name"])
        opp_logo = opponent["logo"]
        del opponent["logo"]
        opp_background = opponent["background"]
        del opponent["background"]
        team, _ = Team.objects.update_or_create(pk=opponent["id"], defaults=opponent)
        self._get_image(opp_logo, team.logo)
        self._get_image(opp_background, team.background)
        self.seen_teams.add(team_id)
        return team

    def _update_location(self, location):
        if location["id"] in self.seen_rinks:
            return Rink.objects.get(pk=location["id"])
        print("Saving Rink: %s" % location["rink_name"])
        rink, _ = Rink.objects.update_or_create(pk=location["id"], defaults=location)
        self.seen_rinks.add(location["id"])
        return rink

    def _update_seasons(self, base_url):
        resp = requests.get(base_url + "/api/seasons")
        assert resp.status_code == 200
        seasons = json.loads(resp.text)

        all_seasons = []
        for season in seasons:
            print("Saving Season: %s" % season["name"])
            season_obj, _ = Season.objects.update_or_create(pk=season["id"], defaults=season)
            all_seasons += [season_obj]
        return all_seasons

    def _update_games(self, base_url, season):
        resp = requests.get(
            "{base}/api/games?season={season}&fulldata".format(base=base_url, season=season.id))
        assert resp.status_code == 200
        games = json.loads(resp.text)
        for game in games:
            game["opponent"] = self._update_opponent(game["opponent"])
            game["location"] = self._update_location(game["location"])
            game["season"] = season
            del game["short_result"]
            Game.objects.update_or_create(pk=game["id"], defaults=game)

    def _update_news(self, base_url):
        resp = requests.get(base_url + "/api/articles/")
        articles = json.loads(resp.text)
        for article in articles:
            print("Saving Article: %s" % article["title"])
            image = article["image"]
            del article["image"]
            del article["teaser"]
            story, _ = NewsStory.objects.update_or_create(pk=article["id"], defaults=article)
            self._get_image(image, story.image)

    def _update_players(self, base_url):
        resp = requests.get(base_url + "/api/players/")
        players = json.loads(resp.text)
        for player in players:
            print("Saving Player: %s %s" % (player["first_name"], player["last_name"]))
            image = player["image"]
            del player["image"]
            headshot = player["headshot"]
            del player["headshot"]
            player_obj, _ = Player.objects.update_or_create(pk=player["id"], defaults=player)
            self._get_image(image, player_obj.image)
            self._get_image(headshot, player_obj.headshot)

    def _update_board(self, base_url):
        resp = requests.get(base_url + "/api/board/")
        members = json.loads(resp.text)
        priority = 1
        for member in members:
            print("Saving Board: %s" % member["last_name"])
            image = member["image"]
            del member["image"]
            member["priority"] = priority
            priority += 1
            board, _ = Board.objects.update_or_create(pk=member["id"], defaults=member)
            self._get_image(image, board.image)

    def _update_coaches(self, base_url):
        resp = requests.get(base_url + "/api/coaches/")
        coaches = json.loads(resp.text)
        priority = 1
        for coach in coaches:
            print("Saving Coach: %s" % coach["last_name"])
            image = coach["image"]
            del coach["image"]
            coach["priority"] = priority
            priority += 1
            coach, _ = Coach.objects.update_or_create(pk=coach["id"], defaults=coach)
            self._get_image(image, coach.image)

    def _update_item(self, base_url, item_id):
        resp = requests.get(base_url + "/api/shop/" + str(item_id))
        item = json.loads(resp.text)
        image = item["image"]
        del item["image"]
        images = item["images"]
        del item["images"]
        opts = item["options"]
        del item["options"]
        cust_opts = item["custom_options"]
        del item["custom_options"]
        item_obj, _ = ShopItem.objects.update_or_create(pk=item_id, defaults=item)
        self._get_image(image, item_obj.image)
        for sec_image in images:
            image = sec_image["image"]
            del sec_image["image"]
            sec_image["shop_item"] = item_obj
            image_obj, _ = ShopItemImage.objects.update_or_create(pk=sec_image["id"],
                                                                  defaults=sec_image)
            self._get_image(image, image_obj.image)
        for opt in opts:
            opt["shop_item"] = item_obj
            ShopItemOptionList.objects.update_or_create(pk=opt["id"], defaults=opt)
        for cust_opt in cust_opts:
            cust_opt["shop_item"] = item_obj
            ShopItemCustomOption.objects.update_or_create(pk=cust_opt["id"], defaults=cust_opt)

    def _update_shop(self, base_url):
        resp = requests.get(base_url + "/api/shop/")
        items = json.loads(resp.text)
        for item in items:
            self._update_item(base_url, item["id"])

    def add_arguments(self, parser):
        parser.add_argument("--base_url", default="https://gthockey.com")

    def handle(self, *args, **options):
        url = options["base_url"]
        if not os.path.exists(self.tmp_img_dir):
            os.mkdir(self.tmp_img_dir)
        try:
            seasons = self._update_seasons(url)
            for season in seasons:
                self._update_games(url, season)
            self._update_news(url)
            self._update_players(url)
            self._update_board(url)
            self._update_coaches(url)
            self._update_shop(url)
        except:  # noqa: E722
            shutil.rmtree(self.tmp_img_dir)
            raise
