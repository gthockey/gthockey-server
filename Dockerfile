FROM python:3.8-alpine

RUN mkdir /www
WORKDIR /www
COPY requirements.txt /www/

RUN apk add \
        gcc \
        libffi-dev \
        linux-headers \
        musl-dev \
        postgresql-client \
        postgresql-dev \
        jpeg-dev \
        zlib-dev
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED 1
COPY . /www/

ENTRYPOINT ["/www/docker.sh"]
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]