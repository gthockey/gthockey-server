# Georgia Tech Hockey Server

[![Build Status](https://travis-ci.org/dgalbraith33/gthockey.svg?branch=master)](https://travis-ci.org/dgalbraith33/gthockey)

This repository holds the backend code for the Georgia Tech Hockey website that can be found [here](https://www.gthockey.com) and the iOS application which can be found on the [App Store](https://apps.apple.com/us/app/gt-hockey/id1484814696).

## Server

The server is written in Django/Python. Configuration is in `gtsite/` and business logic is in
`gthockey/`. The server leverages DjangoRestFramework to send serialize model objects into JSON
for API call responses.

## Setting up the Server

1. Make sure you have python 3 downloaded and installed. To confirm, running the following command in your command line should give the response of something like `Python 3.8.2`
```
python3 --version
```
2. Create a virtual environment
```
virtualenv --python=python3 virtenv
```
3. Activate that virtual environment
```
source virtenv/bin/activate
```
4. Install the necessary dependencies
```
pip install -r requirements.txt
```
5. Set up the database schema
```
./manage.py migrate
```
6. Populate the database from the production website (this can be slow as it fetches images)
```
./manage.py populate_db
```
7. If these steps complete successfully you should be able to run the test server
```
./manage.py runserver
```
